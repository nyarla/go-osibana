package osibana

import (
	"mime"
	"net/http"
	"path/filepath"
)

type AssetsLoaderFunc func(name string) ([]byte, error)

type Static struct {
	Loader AssetsLoaderFunc
}

func NewStatic(loader AssetsLoaderFunc) *Static {
	return &Static{loader}
}

func (static *Static) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	if r.Method != `GET` {
		next(rw, r)
		return
	}

	file := r.URL.Path
	file = file[1:]

	bindata, err := static.Loader(file)
	if err != nil {
		next(rw, r)
		return
	}

	ext := filepath.Ext(file)
	var mimeType string
	if ext != `` {
		mimeType = mime.TypeByExtension(ext)
	}

	if mimeType == `` {
		mimeType = `application/octet-stream`
	}

	rw.Header().Set(`Content-Type`, mimeType)
	rw.Write(bindata)
}
