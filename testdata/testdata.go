package testdata

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"strings"
)

func bindata_read(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	return buf.Bytes(), nil
}

func testdata_hello() ([]byte, error) {
	return bindata_read([]byte{
		0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x00, 0xff, 0xca, 0x48,
		0xcd, 0xc9, 0xc9, 0x57, 0xc8, 0x2f, 0xce, 0x4c, 0x4a, 0xcc, 0x4b, 0x54,
		0xe4, 0x02, 0x04, 0x00, 0x00, 0xff, 0xff, 0x83, 0xb4, 0x99, 0xc7, 0x0f,
		0x00, 0x00, 0x00,
	},
		"testdata/hello",
	)
}

func testdata_style_css() ([]byte, error) {
	return bindata_read([]byte{
		0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x00, 0xff, 0x4a, 0xca,
		0x4f, 0xa9, 0x54, 0xa8, 0x56, 0x48, 0x4a, 0x4c, 0xce, 0x4e, 0x2f, 0xca,
		0x2f, 0xcd, 0x4b, 0xd1, 0x4d, 0xce, 0xcf, 0xc9, 0x2f, 0x52, 0xb0, 0x52,
		0x50, 0x76, 0xb3, 0x00, 0x42, 0x33, 0x05, 0x6b, 0x85, 0x5a, 0x2e, 0x40,
		0x00, 0x00, 0x00, 0xff, 0xff, 0x99, 0xba, 0xa8, 0x81, 0x26, 0x00, 0x00,
		0x00,
	},
		"testdata/style.css",
	)
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		return f()
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() ([]byte, error){
	"testdata/hello": testdata_hello,
	"testdata/style.css": testdata_style_css,
}
