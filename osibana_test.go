package osibana

import (
	"bytes"
	"github.com/codegangsta/negroni"
	"github.com/nyarla/go-osibana/testdata"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestStatic(t *testing.T) {
	resp := httptest.NewRecorder()
	resp.Body = new(bytes.Buffer)

	n := negroni.New()
	n.Use(NewStatic(testdata.Asset))

	req, err := http.NewRequest(`GET`, `/testdata/style.css`, nil)
	if err != nil {
		t.Error(err)
	}

	n.ServeHTTP(resp, req)

	if resp.Code != http.StatusOK {
		t.Errorf(`HTTP response code is not http.StatusOK: (%v)`, resp.Code)
	}

	contentType := resp.Header().Get(`Content-Type`)

	if contentType != `text/css; charset=utf-8` {
		t.Errorf(`HTTP Content-Type is not "text/css; charset=utf-8": (%v)`, contentType)
	}
}

func TestStaticWithUnkownContentType(t *testing.T) {
	resp := httptest.NewRecorder()
	resp.Body = new(bytes.Buffer)

	n := negroni.New()
	n.Use(NewStatic(testdata.Asset))

	req, err := http.NewRequest(`GET`, `/testdata/hello`, nil)
	if err != nil {
		t.Error(err)
	}

	n.ServeHTTP(resp, req)

	if resp.Code != http.StatusOK {
		t.Errorf(`HTTP response code is not http.StatusOK: (%v)`, resp.Code)
	}

	contentType := resp.Header().Get(`Content-Type`)

	if contentType != `application/octet-stream` {
		t.Errorf(`HTTP Content-Type is not "application/octet-stream": (%v)`, contentType)
	}
}
