Osibana
=======

The [negroni](https://github.com/codegangsta/negroni) middleware for static files with [go-bindata](https://github.com/jteeuwen/go-bindata)

How to use
----------

### 1. Compile assets files to Go-lang source code

```bash
# for example
$ go-bindata  -o assets.go -pkg "myapp" -ignore=\\.DS_Store assets/...

```

### 2. Use `osibana` with compiled Go-lang source code

```go
package myapp

import (
    "github.com/codegangsta/negroni"
    "github.com/nyarla/go-osibana"
)

func main() {
    app := negroni.New()
    app.Use( osibana.NewStatic( Asset ) )

    app.Run(`:3000`)
}

```

That's all ;-)

Documentation
-------------

Please see to <http://godoc.org/github.com/nyarla/go-osibana>

Author
------

Naoki OKAMURA (Nyarla) <nyarla@thotep.net>

License
-------

[MIT](http://nyarla.mit-license.org/2014)
